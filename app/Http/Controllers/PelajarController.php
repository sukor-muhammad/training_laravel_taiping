<?php

namespace App\Http\Controllers;

use App\Models\Student;
use App\Models\LevelStudent;
use Illuminate\Http\Request;
use App\Helpers\CustomHelper;
use App\Models\StudentsModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;

class PelajarController extends Controller
{
    public function index(Request $request){


       $lookupCitizen= CustomHelper::lookupCitizen();
       $lookupGender= CustomHelper::lookupGeneral('gender');


        $Student=Student::with(['lookupCitizen','lookupGender','levelStudent'])
        ->filter($request)
        ->paginate(20);

        // dd($Student);


        return view('pelajar.index',compact('Student','lookupCitizen','lookupGender'));

    }
    //


 /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $lookupCitizen= CustomHelper::lookupCitizen();
        $lookupGender= CustomHelper::lookupGeneral('gender');


        return view('pelajar.create',compact('lookupCitizen','lookupGender'));

        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        
        // kalu empty boleh guna nullable "matric_no" =>'nullable',
      $datavalidat=  $request->validate([
         "full_name" =>'required', 
         "identity" => 'required',
         "citizen_type" => 'required',
         "matric_no" => 'required|min:8',
        ],[
            'full_name.required'=>'Nama Penuh Wajib Di isi',
            'citizen_type.required'=>' Wajib Di isi',
         ]);

         $datavalidat['level_session']='1 2023/2024';
    
         DB::beginTransaction();

         try {

            $student= Student::create($datavalidat);

            if($student->id??''){
    
                $datavalidat['student_id']=$student->id;
                LevelStudent::create($datavalidat);
            }
            
            DB::commit();
    
            return redirect()->route('pelajar.index')
            ->withSuccess('Pelajar Created')->with(['name_student'=>$student->full_name]);
          

        } catch (\Throwable $th) {
            
            // dd($th);

            DB::rollback();
           return redirect()->back()->withErrors(['erros'])->withInput();

        }


   


        


        
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

}
