<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;

    protected $table='students';
    // protected $primaryKey='id';
    protected $fillable=['full_name','identity','citizen_type'];
    
    
    /**
     * Get the user that owns the Student
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function lookupCitizen()
    {
        return $this->belongsTo(GeneralLookup::class,
         'citizen_type', 'code');
    }

    public function lookupGender()
    {
        return $this->belongsTo(GeneralLookup::class,
         'gender', 'code');
    }

    public function levelStudent()
    {
        return $this->belongsTo(LevelStudent::class,
         'id', 'student_id');
    }

    public function scopeFilter($query,$filter){

        // citizen_type
       $query->when($filter['citizen_type']??false,function($query,$search){

        //  $query->where('citizen_type','=',$search);
        // $query->where('citizen_type','like',"%$search%");

        $query->where(['citizen_type'=>$search]);
       

       });
        // end citizen_type


         // full_name
       $query->when($filter['full_name']??false,function($query,$search){

         $query->where('full_name','like',"%$search%");


       });
        // end full_name


        // matric
        $query->when($filter['matric_no'] ?? false, function($query, $search){
                    $query->whereHas('levelStudent', function($q) use ($search) {
                        $q->where('matric_no', 'like',"%$search%");
                    });
                }); 

        // end matric








    }


   
    
}
