<?php
namespace App\Helpers;

use App\Models\Role;
use App\Models\GeneralLookup;

class CustomHelper{

    public static function lookupCitizen(){

       $data= GeneralLookup::where(['category'=>'type_citizen'])->get();

       $plucked=$data->pluck('name','code');

    
       return $plucked;


    }


    public static function lookupGeneral($category){

        $data= GeneralLookup::where(['category'=>$category])->get();
 
        $plucked=$data->pluck('name','code');
 
     
        return $plucked;
 
 
     }


     public static function lookupRole(){

      $data= Role::get();

      $plucked=$data->pluck('name','id');

   
      return $plucked;


   }



}