
@extends('layouts.app')
 
@section('title', 'Tambah Pelajar')
 
@section('content')

{{print_r($errors->all())}};

{{-- form --}}
<form class="form form-horizontal" method='POST' action="{{route('pelajar.store')}}" >
  @csrf
    <div class="form-body">
        <div class="row">
            <div class="col-md-4">
                <label for="first-name-horizontal">Full Name</label>
            </div>
            <div class="col-md-8 form-group">
                <input value='{{old('full_name')}}' type="text" id="first-name-horizontal"
                 class="form-control @error('full_name')?? is-invalid @enderror " name="full_name" placeholder="First Name">
               

                @error('full_name')
                    <span class="text-danger">{{$message}}</span>
                @enderror

            </div>
            <div class="col-md-4">
                <label for="identity-horizontal">Identity</label>
            </div>
            <div class="col-md-8 form-group">
                <input  value='{{old('identity')}}' type="text" id="identity-horizontal" class="form-control" name="identity" placeholder="identity">
            </div>
            <div class="col-md-4">
                <label for="contact-info-horizontal">Citizen</label>
            </div>
            <div class="col-md-8 form-group">
               
                <select name="citizen_type" id="contact-info-horizontal"
                 class="form-control @error('citizen_type')?? is-invalid @enderror">
                    <option value="">Sila pilih</option>
                    @php
                        $keycit=$rowcit=null;
                    @endphp
                    @foreach ($lookupCitizen as $keycit=>$rowcit)
                         <option {{ old('citizen_type') == $keycit? 'selected' : '' }} value="{{$keycit}}">{{$rowcit}}</option>
                    @endforeach
                </select>

                @error('citizen_type')
                <span class="text-danger">{{$message}}</span>
                @enderror

            </div>
            <div class="col-md-4">
                <label for="matric-horizontal">No Matric</label>
            </div>
            <div class="col-md-8 form-group">
                <input type="text" id="password-horizontal" class="form-control" name="matric_no" placeholder="Password">
            </div>
            <div class="col-12 col-md-8 offset-md-4 form-group">
                <div class="form-check">
                    <div class="checkbox">
                        <input type="checkbox" id="checkbox1" class="form-check-input" checked="">
                        <label for="checkbox1">Remember Me</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 d-flex justify-content-end">
                <button type="submit" class="btn btn-primary me-1 mb-1">Submit</button>
                <a class="btn btn-light-secondary me-1 mb-1" href="{{route('pelajar.index')}}">Reset</a>
                <button type="reset" class="btn btn-light-secondary me-1 mb-1">Reset</button>
            </div>
        </div>
    </div>
</form>

{{-- end form --}}

@endsection