@extends('layouts.app')
 
@section('title', 'Pelajar')
 
@section('content')


@if (session('success'))
    <div class='alert alert-success'>
        {{session('success')}}
        {{session('name_student')}}

    </div>
@endif


{{-- form --}}
<form class="form form-horizontal" action="{{route('pelajar.index')}}" >
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="first-name-horizontal">Full Name</label>
                                        </div>
                                        <div class="col-md-8 form-group">
                                            <input value='{{request('full_name')}}' type="text" id="first-name-horizontal" class="form-control" name="full_name" placeholder="First Name">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="identity-horizontal">Identity</label>
                                        </div>
                                        <div class="col-md-8 form-group">
                                            <input type="text" id="identity-horizontal" class="form-control" name="identity" placeholder="identity">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="contact-info-horizontal">Citizen</label>
                                        </div>
                                        <div class="col-md-8 form-group">
                                           
                                            <select name="citizen_type" id="contact-info-horizontal" class="form-control">
                                                <option value="">Sila pilih</option>
                                                @php
                                                    $keycit=$rowcit=null;
                                                @endphp
                                                @foreach ($lookupCitizen as $keycit=>$rowcit)
                                                     <option {{ request('citizen_type') == $keycit? 'selected' : '' }} value="{{$keycit}}">{{$rowcit}}</option>
                                                @endforeach
                                            </select>

                                        </div>
                                        <div class="col-md-4">
                                            <label for="matric-horizontal">No Matric</label>
                                        </div>
                                        <div class="col-md-8 form-group">
                                            <input type="password" id="password-horizontal" class="form-control" name="password" placeholder="Password">
                                        </div>
                                        <div class="col-12 col-md-8 offset-md-4 form-group">
                                            <div class="form-check">
                                                <div class="checkbox">
                                                    <input type="checkbox" id="checkbox1" class="form-check-input" checked="">
                                                    <label for="checkbox1">Remember Me</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 d-flex justify-content-end">
                                            <button type="submit" class="btn btn-primary me-1 mb-1">Submit</button>
                                            <a class="btn btn-light-secondary me-1 mb-1" href="{{route('pelajar.index')}}">Reset</a>
                                            <button type="reset" class="btn btn-light-secondary me-1 mb-1">Reset</button>
                                        </div>
                                    </div>
                                </div>
                            </form>

{{-- end form --}}

{{-- button add --}}

<a class='btn btn-info' href="{{route('pelajar.create')}}">Tambah</a>

{{-- button add end--}}


<table class='table'>


    <tr>
        <th>col1</th>
        <th>col1</th>
        <th>col1</th>
        <th>col1</th>
    </tr>

@foreach ($Student as $key=>$item)

    <tr>
       <td>{{$Student->firstItem()+$key}}</td>
        <td>{{$item->full_name??''}}</td>
        <td>{{$item->lookupCitizen->name??''}}</td>
        <td>{{$item->lookupGender->name??''}}</td>
        <td>{{$item->levelStudent->matric_no??''}}</td>
        <td>{{$item->levelStudent->level_semester??''}}</td>
    </tr>

 @endforeach

</table>

{{$Student->appends(request()->except('page'))->links()}}


    

@endsection

@section('scripts')

<script>
@if (session('success'))
Swal.fire({
  position: "top-end",
  icon: "success",
  title: "Your work has been saved",
  showConfirmButton: false,
  timer: 1500
});
@endif



</script>

@endsection

