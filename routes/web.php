<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PelajarController;
use App\Http\Controllers\ProgramController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    echo 'hello world';
    // return view('welcome');
});


Route::get('test1', function () {
     echo 'ini test';
});

// Route::get('pelajar', [PelajarController::class, 'index'])->name('pelajar.index');



Route::middleware(['auth'])->group(function () {

Route::middleware(['rbac:R001,R002'])->group(function () {
    Route::resource('pelajar', PelajarController::class);
});
   
});





Route::resource('program', ProgramController::class);



Auth::routes();

// Auth::routes(['register'=>false]);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
